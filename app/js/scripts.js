
$(document).ready(function(){

//SEARCH FOR LARGE AND MEDIUM VIEWPORTS
	//search field to appear on click on searc icon
	$('.serch-lm-form').on('click', '#search-lm-i', function() {
		$(this).siblings('#search-lm-inp').toggle(500);
	});

    //suggestion on search
	$('#search-lm-inp').on('focus', function() {
		$('#sugg-lm').slideDown(100);
	});
	$('#search-lm-inp').on('submit', function() {
		$('#sugg-lm').slideUp(100);
	});
	$('#search-lm-i').on('click', function() {
		$('#sugg-lm').slideUp(100);
	});

	

//SEARCH FOR SMALL VIEWPORTS
    //search field to appear on click on searc icon
	$('.menu-row').on('click', '#search-s-i', function() {
		$('.wishlist-cart').toggle(0);
		$(this).siblings('#search-s-f').toggle(500);
	});

	//suggestion on search
	$('#search-s-inp').on('focus', function() {
		$('#sugg-s').slideDown(100);
	});
	$('#search-s-inp').on('submit', function() {
		$('#sugg-s').slideUp(100);
	});


//LOGIN FORM
	$(document).click( function(event){
	    if( $(event.target).closest(".login-form").length ) 
	    return;
	    $(".login-form").fadeOut(300);
	    event.stopPropagation();
    });
	$('.login').on('click', '#login', function() {
		$(this).siblings('.login-form').fadeIn(300);
		return false;
	});
	$('.login-form').on('submit', function() {
		$('.login-form').fadeOut(300);
	});


//REGISTER FORM
    $('#register').on('click', function() {
		$('.register-underlayer').fadeIn(200);
		$('.register-form').fadeIn(500);
	});
	$('.register-underlayer').on('click', function() {
        $('.register-form').fadeOut(300);
		$('.register-underlayer').fadeOut(500);
	});
	$('.register-form').on('submit', function() {
		$('.register-form').fadeOut(300);
	});



//MAIN NAV FOR SMALL VIEWPORTS
    $('#menu-s-i').click(function() {
    	var nav = $('.main-nav');
    	var icon = $('#menu-s-i');
    	nav.toggle(500);
    	if (icon.hasClass('fa-bars')) {
    		icon.addClass('fa-times');
    		icon.removeClass('fa-bars');
    	} else {
    		icon.addClass('fa-bars');
    		icon.removeClass('fa-times');
    	}
    });


//SLIDER
    $('.slider').slick({
        dots: true,
        infinite: true,
        speed: 1500,
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 767,
            settings: {
              dots: false
          }
        },
        {
            breakpoint: 539,
            settings: 'unslick'
        },
      ]
    });


//EMAIL VALIDATION
    var emailPattern = /^[a-z0-9_-]+@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i;

    //for newsletter
    var emailN = $('#email-newslet');
    emailN.focus(function() {
    	$('#news-tria').fadeIn(300);
        $('#ex-msg').fadeIn(300);
        $('#ex-msg').text('Enter email using example: user@email.com');
    });
    emailN.blur(function() {
        $('#ex-msg').fadeOut(300);
        $('#news-tria').fadeOut(300);
    });
    emailN.blur(function() {
    	if(emailN.val().search(emailPattern) !== 0){
             $('.submit-btn').attr('disabled', true);
             $('#news-tria').fadeIn(300);
             $('#ex-msg').fadeIn(300);
             $('#ex-msg').text('Please enter correct email: user@email.com');
        } else{
             $('.submit-btn').attr('disabled', false);
        }
    });

    //for login form
    var emailL = $('#email-login');
    emailL.blur(function() {
    	if(emailL.val().search(emailPattern) !== 0){
             $('.submit-btn').attr('disabled', true);
             $('#log-tria').fadeIn(0);
             $('#log-error-msg').text('Please enter correct email: user@email.com');
             $('#log-error-msg').css('padding', '5px');
        } else{
             $('.submit-btn').attr('disabled', false);
             $('#log-tria').fadeOut(0);
             $('#log-error-msg').text('');
             $('#log-error-msg').css('padding', '0');
        }
    });

    //for register form
    var emailR = $('#email-reg');
    emailR.blur(function() {
    	if(emailR.val().search(emailPattern) !== 0){
             $('.submit-btn').attr('disabled', true);
             $('#reg-tria').fadeIn(0);
             $('#reg-error-msg').text('Please enter correct email: user@email.com');
             $('#reg-error-msg').css('padding', '5px');
        } else{
             $('.submit-btn').attr('disabled', false);
             $('#reg-tria').fadeOut(0);
             $('#reg-error-msg').text('');
             $('#reg-error-msg').css('padding', '0');
        }
    });

    //validation for password identity
    $('#pass-2').blur(function(){
    	if($('#pass-2').val() !== $('#pass-1').val()) {
    		$('.submit-btn').attr('disabled', true);
    		$('#pass-tria').fadeIn(0);
            $('#pass').text('Passwords should be identical');
            $('#pass').css('padding', '5px');
    	}
    	else {
    		$('#pass-tria').fadeOut(0);
            $('#pass').text('');
            $('#pass').css('padding', '0');
            $('.submit-btn').attr('disabled', false);
    	}
    });

    //remove validation error messages on reset
    $('#reset').click(function(){
        $('#reg-tria').fadeOut(0);
        $('#reg-error-msg').text('');
        $('#reg-error-msg').css('padding', '0');
        $('#pass-tria').fadeOut(0);
        $('#pass').text('');
        $('#pass').css('padding', '0');
    });



//BLOG NEWS
    $('.blog-news').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        vertical: true,
        speed: 1000
    });



//FLICKR WIDGET
   $('#div-1').click(function(){
   	    $('#div-1').toggleClass('pop-up');
   });
   $('#div-2').click(function(){
   	    $('#div-2').toggleClass('pop-up');
   });
   $('#div-3').click(function(){
   	    $('#div-3').toggleClass('pop-up');
   });
   $('#div-4').click(function(){
   	    $('#div-4').toggleClass('pop-up');
   });
   $('#div-5').click(function(){
   	    $('#div-5').toggleClass('pop-up');
   });
   $('#div-6').click(function(){
   	    $('#div-6').toggleClass('pop-up');
   });

});