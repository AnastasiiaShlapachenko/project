
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var maps = require('gulp-sourcemaps');
var del = require('del');
var sync = require('browser-sync');
var autopref = require('gulp-autoprefixer');
var cssMin = require('gulp-clean-css');
var minImg = require('gulp-imagemin');



//live stream in browser
gulp.task('browserSync', function () {
	sync({
		server: {
			baseDir: 'app'
		}
	})
});



//task to compress imgs
gulp.task('compressImg', function () {
	gulp.src('app/images/*')
	.pipe(minImg())
	.pipe(gulp.dest('app/images/img'));
});



// JS TASKS
//for home page
gulp.task('concatScriptsHome', function() {
	return gulp.src(['app/js/jquery-3.3.1.js', 'app/js/slick.js', 'app/js/scripts.js'])
	.pipe(maps.init())
	.pipe(concat('home.js'))
	.pipe(maps.write('./'))
	.pipe(gulp.dest('app/js'))
	.pipe(sync.reload({stream:true}));
});

gulp.task('minifyJSHome', ['concatScriptsHome'], function () {
	return gulp.src('app/js/home.js')
	.pipe(uglify())
	.pipe(rename('home.min.js'))
	.pipe(gulp.dest('app/js'));
});

//for products page
gulp.task('concatScriptsProd', function() {
	return gulp.src(['app/product-page/js/jquery-3.3.1.js', 'app/product-page/js/slick.js', 'app/product-page/js/scripts.js'])
	.pipe(maps.init())
	.pipe(concat('prod.js'))
	.pipe(maps.write('./'))
	.pipe(gulp.dest('app/product-page/js'))
	.pipe(sync.reload({stream:true}));
});

gulp.task('minifyJSProd', ['concatScriptsProd'], function () {
	return gulp.src('app/product-page/js/prod.js')
	.pipe(uglify())
	.pipe(rename('prod.min.js'))
	.pipe(gulp.dest('app/product-page/js'));
});




//STYLES TASKS
//for home page
gulp.task('sassToCssHome', function () {
	return gulp.src('app/styles/scss/style.scss')
	.pipe(maps.init())
	.pipe(sass())
	.pipe(autopref(['last 10 versions']))
	.pipe(maps.write('./'))
	.pipe(gulp.dest('app/styles/css'))
	.pipe(sync.reload({stream:true}));
});

gulp.task('minifyCssHome', ['sassToCssHome'], function () {
	return gulp.src('app/styles/css/style.css')
	.pipe(cssMin())
	.pipe(rename('style.min.css'))
	.pipe(gulp.dest('app/styles/css'));
});


//for products page
gulp.task('sassToCssProd', function () {
	return gulp.src('app/product-page/styles/scss/prod-style.scss')
	.pipe(maps.init())
	.pipe(sass())
	.pipe(autopref(['last 10 versions']))
	.pipe(maps.write('./'))
	.pipe(gulp.dest('app/product-page/styles/css'))
	.pipe(sync.reload({stream:true}));
});

gulp.task('minifyCssProd', ['sassToCssProd'], function () {
	return gulp.src('app/product-page/styles/css/prod-style.css')
	.pipe(cssMin())
	.pipe(rename('prod-style.min.css'))
	.pipe(gulp.dest('app/product-page/styles/css'));
});



//WATCH CHANGES LIVE
//for home page
gulp.task('watchHome', ['concatScriptsHome', 'sassToCssHome', 'browserSync'], function () {
	gulp.watch('app/styles/scss/**/*.scss', ['sassToCssHome']);
	gulp.watch('app/js/*.js', ['concatScriptsHome']);
	gulp.watch('app/index.html', sync.reload);
});

//for products page
gulp.task('watchProd', ['concatScriptsProd', 'sassToCssProd', 'browserSync'], function () {
	gulp.watch('app/product-page/styles/scss/**/*.scss', ['sassToCssProd']);
	gulp.watch('app/product-page/js/*.js', ['concatScriptsProd']);
	gulp.watch('app/product-page/product-page.html', sync.reload);
});



//building a folder with final files
gulp.task('clean', function () {
	del(['dist', 'app/styles/css/style*.css*', 'app/product-page/styles/css/*style*.css*', 'app/js/home*.js*', 'app/product-page/js/prod*.js*']);
});

gulp.task('build', [
	'minifyJSHome', 'minifyJSProd', 'minifyCssHome', 'minifyCssProd'], function () {
	return gulp.src([
		'app/index.html',
		'app/styles/css/style.min.css',
		'app/js/home.min.js',
		'app/product-page/product-page.html',
		'app/product-page/styles/css/prod-style.min.css',
		'app/product-page/js/prod.min.js',
		'app/images/**',
		'app/fonts/**'], { base: './'})
	.pipe(gulp.dest('dist'));
});

gulp.task('default', ['clean'], function() {
	gulp.start('build');
});